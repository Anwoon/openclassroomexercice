import { Component, Input } from '@angular/core';
import { Post } from '../../interfaces/post';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.css']
})
export class PostListItemComponent {

  @Input() post: Post;

  constructor() { }

  likeIt() {
    this.post.loveIts++;
  }

  dontLikeIt() {
    this.post.loveIts--;
  }

}
