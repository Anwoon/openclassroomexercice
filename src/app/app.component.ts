import { Component } from '@angular/core';
import { Post } from '../interfaces/post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Posts';
  posts: Post[] = [
    {
      title: 'Mon premier post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' +
        ' Nam sed mi pulvinar, convallis ex nec, facilisis velit. Sed ullamcorper convallis mi. Ut gravida dignissim scelerisque.',
      loveIts: 1,
      created_at: new Date()
    },
    {
      title: 'Mon deuxième post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' +
        ' Nam sed mi pulvinar, convallis ex nec, facilisis velit. Sed ullamcorper convallis mi. Ut gravida dignissim scelerisque.',
      loveIts: -1,
      created_at: new Date()
    },
    {
      title: 'Encore un post',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.' +
        ' Nam sed mi pulvinar, convallis ex nec, facilisis velit. Sed ullamcorper convallis mi. Ut gravida dignissim scelerisque.',
      loveIts: 0,
      created_at: new Date()
    },
  ];
}
